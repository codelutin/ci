# Templates pour la CI GitLab

Le dossier [swarm/](./swarm/) contient des templates pour le build et le
déploiement via swarm-deployer, ainsi que son propre README.

## Build

_`build*` sont les nouvelles versions de `swarm/build*`_

### Template `build.yml`

Ce template est le plus simple à utiliser, mais permet d'avoir seulement un job
de build par projet.

À minima, il faut un stage `dockerbuild` et un `Dockerfile` à la racine du
dépôt. Voici un exemple de `.gitlab-ci.yml` minimaliste:

```yaml
include:
- project: codelutin/ci
  file: build.yml

stages:
  - dockerbuild
```

### Template `build_base.yml`

Le template `build_base.yml` est la base de  `build.yml`. On peut utiliser
directement `build_base.yml` pour avoir plus de flexibilité. Par exemple, ça
permet d'avoir plusieurs jobs de build dans un même `.gitlab-ci.yml`:

```yaml
include:
- project: codelutin/ci
  file: build_base.yml

stages:
  - dockerbuild

webclient:
  extends: .build
  variables:
    IMAGE_TAG: webclient
    DOCKERFILE: Dockerfile.client

server:
  extends: .build
  variables:
    IMAGE_TAG: server
    DOCKERFILE: Dockerfile.server
```

`IMAGE_TAG` est arbitraire. On peut par exemple utiliser `webclient-$CI_COMMIT_SHORT_SHA` pour avoir des tags différents pour chaque commit.

### Variables supportées

| Variable              | Requis ? | Description                                       | Valeur par défaut              |
| --------------------- | -------- | ------------------------------------------------- | ------------------------------ |
| `DOCKER_BUILD_DIR`    | Non      | Répertoire racine à utiliser                      | `.`                            |
| `DOCKERFILE`          | Non      | Dockerfile à utiliser                             | `$DOCKER_BUILD_DIR/Dockerfile` |
| `IMAGE_PATH`          | Non      | Chemin de l'image                                 | vide                           |
| `IMAGE_TAG`           | Non      | Tag de l'image                                    | `$CI_COMMIT_SHORT_SHA`         |
| `EXTRA_IMAGE_TAG`     | Non      | Tag additionnel                                   | vide                           |
| `EXTRA_BUILD_OPTIONS` | Non      | Options supplémentaires à passer à `docker build` | vide                           |
| `BLOCK_ON`            | Non      | Niveau à partir duquel bloquer le build           | vide                           |

Le nom de l'image est construit ainsi: `IMAGE_NAME="${CI_REGISTRY_IMAGE}$IMAGE_PATH:$IMAGE_TAG"`

### Variables exportées

Les variables suivantes sont exportées en tant que [report dotenv](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv):

| Variable             | Quand ?                         | Description                      |
| -------------------- | ------------------------------- | -------------------------------- |
| `IMAGE_NAME`         | Toujours                        | Nom de l'image construite        |
| `IMAGE_DIGEST`       | Toujours                        | SHA256 de l'image construite     |
| `EXTRA_IMAGE_NAME`   | Si `EXTRA_IMAGE_TAG` est défini | Nom de l'image supplémentaire    |
| `EXTRA_IMAGE_DIGEST` | Si `EXTRA_IMAGE_TAG` est défini | SHA256 de l'image supplémentaire |

## Déploiement sur demo4

`deploy_demo.yml`

### Variables supportées

| Variable          | Requis ? | Description                | Valeur par défaut |
| ----------------- | -------- | -------------------------- | ----------------- |
| `COMPOSE_FILE`    | Oui      | Fichier Compose à utiliser | vide              |
| `DEPLOYMENT_NAME` | Oui      | Nom du déploiement         | vide              |


## ToDo

* Renommer `ci.yml` en `maven.yml`
* Déplacer le contenu du dossier swarm à la racine
* Échouer le job `deploy-demo` en cas d'échec du déploiement
