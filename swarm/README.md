# Templates pour factoriser les .gitlab-ci.yml des services du swarm

## Déploiement

Le template [swarm-deployer.yml](swarm-deployer.yml) permet d'ajouter un job qui
crée un zip et l'envoie vers <https://swarm-deployer.cloud.codelutin.com>. Pour
utiliser le template, le projet doit contenir:

* Un fichier `docker-compose.yml`
* Optionnellement, des dossiers `scripts`, `configs`, `secrets` et `envfiles`

Voici un exemple de `.gitlab-ci.yml` minimaliste:

```yaml
stages:
  - deploy

variables:
  TARGET: infracl # 'infracl', 'libre-entreprise' ou 'saas'

include:
  - project: 'codelutin/ci'
    file: '/swarm/swarm-deployer.yml'
```

### Variables supportées

| Variable     | Requis ? | Description                                                                                                                       | Valeur par défaut |
| ------------ | -------- | --------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| `TARGET`     | Oui      | «catégorie» sur swarm-deployer. Il faut mettre `FSN1`, `HEL1` ou `global` (anciennement  `infracl`, `libre-entreprise` ou `saas`) | pas définie       |
| `DEPLOY_DIR` | Non      | Dossier contenant le fichier `docker-compose.yml` et les dossiers `scripts`, `configs`, `secrets` et `envfiles`                   | `.`               |

### Variables d'environnement dans le fichier compose

Le fichier compose peut contenir des variables d'environnement. Ces variables
seront substituées par `envsubst` par des [variables
GitLab](https://docs.gitlab.com/ee/ci/yaml/README.html#variables) (soit des
variables custom, soit des variables prédéfinies). Exemple:
```yaml
  swarm-deployer:
    image: registry.nuiton.org/codelutin/ci
    networks:
      - default
      - caddy
    environment:
      SECRET_KEY: "$SECRET_KEY"
```
Ici, $SECRET_KEY sera remplacée dans le fichier compose avant que celui-ci ne
soit ajouté au zip.

Si on souhaite ajouter des variables sans qu'elles ne soient substituées par
`envsubst`, il faut utiliser un hack. Par exemple:
```yaml
healthcheck:
  test: pg_isready -q -d $${EMPTY}POSTGRES_DB -U $${EMPTY}POSTGRES_USER || exit 1
```
Ici, `envsubst` va remplacer `${EMPTY}` par une chaine vide. Le fichier ajouté
au zip contiendra donc:
```yaml
healthcheck:
  test: pg_isready -q -d $POSTGRES_DB -U $POSTGRES_USER || exit 1
```

## Build (deprecated)

Deux templates sont disponibles pour factoriser le build. `build.yml` est le
plus simple à utiliser, mais permet d'avoir seulement un job de build par
projet.

Voici un exemple de `.gitlab-ci.yml` minimaliste:

```yaml
stages:
  - build

include:
  - project: 'codelutin/ci'
    file: '/swarm/build.yml'
```

### Variables supportées

| Variable                      | Requis ? | Description                                    | Valeur par défaut                                                    |
| ----------------------------- | -------- | ---------------------------------------------- | -------------------------------------------------------------------- |
| `DOCKER_BUILD_DIR`            | Non      | Répertoire racine contenant le Dockerfile      | `.`                                                                  |
| `DOCKERFILE`                  | Non      | Dockerfile à utiliser                          | `$DOCKER_BUILD_DIR/Dockerfile`                                       |
| `IMAGE_NAME` **(deprecated)** | Non      | Nom de l'image (sera remplacé par `IMAGE_TAG`) | `$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG/$CI_DEFAULT_BRANCH/latest}` |
| `EXTRA_IMAGE_TAG`             | Non      | Nom additionnel                                | vide                                                                 |

### Template `build_base.yml`

Le template `build_base.yml` permet d'avoir plusieurs jobs de build dans un même
`.gitlab-ci.yml`. Exemple:

```yaml
stages:
  - build
  - deploy

variables:
  TARGET: infracl

webclient:
  extends: .build
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE:webclient
    DOCKERFILE: Dockerfile.client
server:
  extends: .build
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE:server
    DOCKERFILE: Dockerfile.server

include:
  - project: 'codelutin/ci'
    file:
      - '/swarm/swarm-deployer.yml'
      - '/swarm/build_base.yml'
```
